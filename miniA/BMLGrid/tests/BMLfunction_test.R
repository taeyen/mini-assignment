library(BMLgrid)

#refer Nick's test codes
createBML_test1 = function()
	#check createdBMLGrid works correctly
	#when number of cars are greater than grid space
{
	cat('Running createdBMLGrid test 1 ... \n')
	initial = createdBMLGrid(10, 10, list(red = 100, blue = 100))
	if(exists('initial')) stop('Error in createdBMLGrid Function')
}

createBML_test1()

createBML_test2 = function()
	#check createdBMLGrid works correctly
	#when their is no car
{
	cat('Running createdBMLGrid test 2 ... \n')
	initial = createdBMLGrid(10, 10, list(red = 0, blue = 0))
	if(exists('initial')) stop('Error in createdBMLGrid Function')
}

createBML_test2()

moveCars_test = function()
	#check moveCarHorizon works correctly to move each cars
{	
	cat('Running moveCarHorizon test ... \n')
	g = matrix(c(0,2,0,2,2,1,1,1,0) ,nrow = 3, ncol = 3)
	g1 = matrix(c(1,2,0,2,2,0,0,1,1), nrow = 3, ncol = 3)
	g2 = matrix(c(1,0,2,2,0,2,0,1,1), nrow = 3, ncol = 3)
	tmp = moveCarHorizon(g, 1, 2)
	a1 = tmp$grid
	if(sum(g1 == a1) != 9){ stop('Error in moveCarHorizon')}
	tmp2 = moveCarHorizon(t(a1), 2, 1)
	a2 = t(tmp2$grid)
	if(sum(g2 == a2) != 9){ stop('Error in t(moveCarHorizon)')} 
}

moveCars_test()